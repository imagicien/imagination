__author__ = 'carl.lemaire@usherbrooke.ca'

from imagination import *
from math import *


def mer_agitee():
    info(
        nom="MerAgitee",
        fps=30,
        t_fin=4,
        largeur=960,
        hauteur=540,
        avconv="avconv -r %1 -i %2 -b:v 1000k %3"
    )

    funcs({
        "sin1": lambda t: sin(pi/4*t),
        "cos2": lambda t: cos(pi/6*t),
        "sin3": lambda t: sin(pi/10*t),
        "cos4": lambda t: cos(pi/16*t),
        "houle1": lambda t: 0.2*sin(pi/4*t),
        "houle2": lambda t: 0.15*cos(pi/5*t),
        "houle3": lambda t: 0.1*sin(pi/6*t),
        "houle4": lambda t: 0.05*cos(pi/7*t),
        "vag1": lambda t: 0.08*sin(pi*t),
        "vag2": lambda t: 0.08*cos(pi*t),
        "coucher": lambda t: 0.5/24 * t
    })

    eau1 = Couleur(0/255, 119/255, 164/255)
    eau2 = Couleur(35/255, 82/255, 100/255)
    eau3 = Couleur("1B3A46")
    eau4 = Couleur(24/255, 34/255, 38/255)

    def vagues(n):
        x = -0.8
        for i in range(-int(n/2), int(n/2)+3):
            t_vag = T(translate=(0, "vag1")) if i % 2 == 0 else T(translate=(0, "vag2"))
            with T(translate=(x + i*0.7, 0)), t_vag:
                draw(Figure.cercle, 1)

    # Scène:

    set_couleur(Couleur("EA2418"))
    with T(translate=(0, "coucher")):
        draw(Figure.cercle, 0.9)

    with T(translate=(0, 0.15)):
        set_couleur(eau4)
        with T(scale=0.3, translate=("cos4", 0)), T(translate=(0, "houle4")):
            vagues(16)
        set_couleur(eau3)
        with T(scale=0.6, translate=("sin3", 0.2)), T(translate=(0, "houle3")):
            vagues(12)
        set_couleur(eau2)
        with T(scale=0.8, translate=("cos2", 0.35)), T(translate=(0, "houle2")):
            vagues(10)
        set_couleur(eau1)
        with T(translate=("sin1", 0.5)), T(translate=(0, "houle1")):
            vagues(8)

mer_agitee()
render()