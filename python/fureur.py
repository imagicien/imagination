from imagination import *
from math import *


def fureur():
    info(
        nom="fureurPy",
        fps=30,
        t_fin=8,
        largeur=960,
        hauteur=540,
        avconv="avconv -r %1 -i %2 -b:v 1000k %3"
    )

    funcs({
        "90*t": lambda t: 90 * t,
        "45*t": lambda t: 45 * t,
        "sin": lambda t: 90 * sin(2*pi*t),
        "sin0.2": lambda t: 1 + 0.2 * sin(2*pi*t),
        "cos0.2": lambda t: 0.7 + 0.2 * sin(2*pi*t + pi)
    })

    noir = Couleur(0, 0, 0)
    blanc = Couleur(1, 1, 1)
    rouge = Couleur(1, 0, 0)

    # Scène:

    fill(rouge)

    blanc.set()
    with T(scale="sin0.2"):
        draw(Figure.cercle, taille=0.9)

    noir.set()
    with T(scale="cos0.2", rotate="90*t"):
        for i in range(0, 4):
            with T(rotate=90 * i):
                with T(translate=(0, -0.25 + 0.14)):
                    draw(Figure.rect, taille=(0.14, 0.5))

                with T(translate=(0, -0.5 + 0.14)), T(rotate="sin"), T(translate=(0.25 - 0.07, 0)):
                    draw(Figure.rect, taille=(0.5, 0.14))

fureur()
render()