#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'carl.lemaire@usherbrooke.ca'

from enum import Enum
import os
import subprocess
import struct

donnees = []
info_dict = {}


class Couleur(object):
    """
    Couleur
    """

    def __init__(self, *args):
        self.a = 1
        self.r = 0
        self.g = 0
        self.b = 0

        if len(args) == 3:
            self.r, self.g, self.b = args
        elif type(args[0]) is str:
            self.r, self.g, self.b = struct.unpack('BBB', bytes.fromhex(args[0]))
            self.r /= 255
            self.g /= 255
            self.b /= 255

    def set(self):
        set_couleur(self)

    @property
    def tuple(self):
        return self.a, self.r, self.g, self.b


class Figure(Enum):
    rect = "rect"
    cercle = "cercle"


def ajouter(action, *args):
    global donnees
    donnees += [action, len(args)]
    if len(args) != 0:
        donnees += args


def info(**args):
    global info_dict
    info_dict = args

    dict_list = []
    for cle, valeur in args.items():
        dict_list.append(cle)
        dict_list.append(valeur)
    ajouter("Info", *dict_list)


def funcs(dict_f):
    # Évaluer les lambdas
    fps = info_dict["fps"]
    t_fin = info_dict["t_fin"]
    dt = 1.0 / fps

    data = {}
    for nom, lamb in dict_f.items():
        data[nom] = []
        # TODO: Utiliser numpy.linspace?
        t = 0
        while t < t_fin:
            data[nom].append(lamb(t))
            t += dt

    # Envoyer
    for nom, tableau in data.items():
        ajouter("Func", nom, *[round(x, 4) for x in tableau])


def fill(couleur):
    ajouter("Fill", *couleur.tuple)


def set_couleur(couleur):
    ajouter("SetCouleur", *couleur.tuple)


def draw(figure, taille, position=(0, 0)):
    taille = taille if type(taille) is tuple else (taille, taille)
    ajouter("Draw", figure.value, taille[0], taille[1], position[0], position[1])


def save():
    ajouter("Save")


def restore():
    ajouter("Restore")


def apply_transform(transform):
    ajouter("Transform", *transform.tuple)


def somme_avec_funcs(liste):
    """
    Sommer les éléments, inclure references a fonctions
    Exemple: f1(t) + f2(t) + x  -->  $f1$f2$x
    """
    somme = 0
    chaine = ""
    for el in liste:
        if type(el) is str:
            chaine += "$" + el
        else:
            somme += el

    if chaine != "":
        return chaine + "$" + str(somme)
    else:
        return somme


def mult_avec_funcs(liste):
    """
    Multiplier les éléments, inclure references aux fonctions
    Exemple: f1(t) * f2(t) * x  -->  ?f1?f2?x
    """
    produit = 1
    chaine = ""
    for el in liste:
        if type(el) is str:
            if el.contains("?"):
                raise ValueError
            chaine += "?" + el
        else:
            produit *= el

    if chaine != "":
        return chaine + "?" + str(produit)
    else:
        return produit


class T(object):
    """
    Transformation géométrique
    """

    def __init__(self, translate=None, rotate=None, scale=None):
        translate = (0, 0) if translate is None else translate
        rotate = 0 if rotate is None else rotate
        scale = (1, 1) if scale is None else scale

        self.translate = translate if type(translate) is list else [translate]
        self.rotate = rotate if type(rotate) is list else [rotate]

        if type(scale) is list:
            self.scale = scale
        elif type(scale) is tuple:
            self.scale = [scale]
        else:
            self.scale = [(scale, scale)]

    def __enter__(self):
        save()
        apply_transform(self)

    def __exit__(self, exc_type, exc_val, exc_tb):
        restore()

    @property
    def tuple(self):
        trans = self.translate
        rotate = self.rotate
        scale = self.scale

        # Sommer les translations
        trans_x, trans_y = zip(*trans)  # Séparer les tuples
        trans_sum = somme_avec_funcs(trans_x), somme_avec_funcs(trans_y)

        # Sommer les rotations
        rotate_sum = somme_avec_funcs(rotate)

        # Sommer les échelles
        scale_x, scale_y = zip(*scale)  # Séparer les tuples
        scale_sum = mult_avec_funcs(scale_x), mult_avec_funcs(scale_y)

        # transX, transY, rotate, scaleX, scaleY
        return trans_sum + (rotate_sum,) + scale_sum


def render(debug_and_progress=True, reopen_image_viewer=False):
    print(donnees)

    donnees_str = [str(d) for d in donnees]
    pargs = ["../out/QDebug/imagination", "../.."] + donnees_str

    if debug_and_progress:
        subprocess.check_call(pargs)
    else:
        subprocess.check_output(pargs, input=b"a")

    if reopen_image_viewer:
        os.system("pkill eom")
        subprocess.Popen("eom ../../Images/%s_000000.png" % info_dict["nom"], shell=True)