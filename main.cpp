#include <iostream>
#include <streambuf>

#include <QVector>
#include <QMap>
#include <QList>
#include <QStringList>
#include <QFile>
#include <QDir>
#include <QProcess>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include "SkCanvas.h"
#include "SkData.h"
#include "SkForceLinking.h"
#include "SkGraphics.h"
#include "SkSurface.h"
#include "SkImage.h"
#include "SkStream.h"

__SK_FORCE_IMAGE_DECODER_LINKING;

struct Instruction
{
    QString nom;
    QStringList args;
};

void charger_donnees(int argc, char** argv);

void dessiner(SkCanvas* canvas, double t, double t_fin);
void draw_value(SkCanvas* canvas, SkPaint paint, QJsonValue value, double t);
void draw_array(SkCanvas* canvas, SkPaint paint, QJsonArray array, double t);
void draw_object(SkCanvas* canvas, SkPaint paint, QJsonObject object, double t);
void draw_figure(SkCanvas* canvas, SkPaint paint, QJsonObject figure_object, double t);
void draw_gen(SkCanvas* canvas, SkPaint paint, QJsonObject gen_object, double t);

void vider_dossier_images();
SkColor color_from_args(QStringList args);
QVector<double> doubles_from_args(QStringList args);
QVector<double> doubles_from_args(QStringList args, int skip);
double parse_value(QString chaine);
void erreur_fatale(QString message);

QString dossier_travail;
QMap<QString, QString> info;
QList<Instruction> actions;
QMap<QString, QVector<double>> funcs;
int i_image;

int main(int argc, char** argv)
{
    /*
     * Paramètres:
     *      0. (executable Imagination)
     *      1. Chemin vers dossier parent de src    ex.: ../../..
     *      2+ Donnees (actions)
    */

    //DEBUG
    std::cout << "Press a key" << std::endl;
    std::cin.get();

    dossier_travail = argv[1];

    //charger donnees
    charger_donnees(argc, argv);

    int largeur = info["largeur"].toInt();
    int hauteur = info["hauteur"].toInt();
    int demi_larg = largeur/2;
    int demi_haut = hauteur/2;
    QString nom = info["nom"];

    //init skia
    SkAutoGraphics ag;

    //creer surface
    SkAutoTUnref<SkSurface> surface(SkSurface::NewRasterN32Premul(largeur, hauteur));

    //mettre l'origine au centre
    surface->getCanvas()->translate(demi_larg, demi_haut);

    //vider dossier Images
    vider_dossier_images();

    //produire les images
    std::cout << "Dessin en cours..." << std::endl;

    double t = 0;
    double fps = info["fps"].toDouble();
    double dt = 1.0/fps; //secondes
    double t_fin = info["t_fin"].toDouble();
    i_image = 0;
    for(; t < t_fin; t += dt)
    {
        //faire les dessins
        dessiner(surface->getCanvas(), t, t_fin);

        //creer image (bitmap)
        SkAutoTUnref<SkImage> image(surface->newImageSnapshot());
        //extraire data (encode)
        SkAutoDataUnref data(image->encode());
        if (NULL == data.get()) {
            return false;
        }
        //ecrire fichier
        QString filename = QString("%1/Images/%2_%3.png")
                .arg(dossier_travail)
                .arg(nom)
                .arg(i_image, 6, 10, QChar('0'));
        SkFILEWStream stream(filename.toUtf8().constData());
        if(!stream.write(data->data(), data->size()))
            return 1;

        i_image++;
        std::cout << "\r" << t << "/" << t_fin << "    ";
        std::flush(std::cout);
    }

    //generer video
    QString fichier_video = QString("%1/Video/%2.mp4").arg(dossier_travail, nom);
    QFile::remove(fichier_video);

    QString av_input = QString("%1/Images/%2_%06d.png").arg(dossier_travail, nom);

    std::cout << std::endl << "Encodage en cours..." << std::endl;
    QProcess* avconv = new QProcess();
    QString commande = QString(info["avconv"])
            .arg(fps).arg(av_input).arg(fichier_video);
    avconv->start(commande);

    if(avconv->waitForFinished())
        std::cout << "Encodage termine." << std::endl;
    else
        std::cout << "Encodage: echec." << std::endl;

    return 0;
}

void charger_donnees(int argc, char **argv)
{
    Instruction info_instr;
    for(int i = 2; i < argc;) // On commence a 2, voir main
    {
        Instruction instr;
        instr.nom = argv[i++];
        int nb_args = QString(argv[i++]).toInt();
        int fin_args = i + nb_args;
        for(; i < fin_args; i++)
        {
            instr.args.append(argv[i]);
        }

        if(instr.nom == "Info")
            info_instr = instr;
        else
            actions.append(instr);
    }

    //charger info
    QStringList::iterator i = info_instr.args.begin();
    while(i != info_instr.args.end())
    {
        QString cle = *(i++);
        QString valeur = *(i++);
        info[cle] = valeur;
    }
}

void dessiner(SkCanvas* canvas, double t, double t_fin)
{
    int l_unit = info["hauteur"].toInt();

    canvas->drawColor(SK_ColorBLACK);

    // Paint blanche antialiased
    SkPaint paint;
    paint.setStyle(SkPaint::kFill_Style);
    paint.setFlags(paint.getFlags() | SkPaint::kAntiAlias_Flag);
    paint.setColor(SK_ColorWHITE);

    for(Instruction instr : actions)
    {
        if(instr.nom == "Save")
            canvas->save();
        else if(instr.nom == "Restore")
            canvas->restore();
        else if(instr.nom == "Fill")
            canvas->drawColor(color_from_args(instr.args));
        else if(instr.nom == "SetCouleur")
            paint.setColor(color_from_args(instr.args));
        else if(instr.nom == "Draw")
        {
            QString figure = instr.args[0];
            int w = instr.args[1].toDouble() * l_unit;
            int h = instr.args[2].toDouble() * l_unit;
            int x = instr.args[3].toDouble() * l_unit;
            int y = instr.args[4].toDouble() * l_unit;
            SkRect bounds = SkRect::MakeXYWH(x-w/2, y-h/2, w, h);
            if(figure == "rect")
                canvas->drawRect(bounds, paint);
            else if(figure == "cercle")
                canvas->drawOval(bounds, paint);
            else
                erreur_fatale("Figure non reconnue.");
        }
        else if(instr.nom == "Transform")
        {
            QString trans_x_str = instr.args[0];
            QString trans_y_str = instr.args[1];
            QString rotate_str = instr.args[2];
            QString scale_x_str = instr.args[3];
            QString scale_y_str = instr.args[4];
            double trans_x = parse_value(trans_x_str) * l_unit;
            double trans_y = parse_value(trans_y_str) * l_unit;
            // TODO: Optimisation
            canvas->translate(trans_x, trans_y);
            canvas->rotate(parse_value(rotate_str));
            canvas->scale(parse_value(scale_x_str), parse_value(scale_y_str));
        }
        else if(instr.nom == "Func")
        {
            funcs[instr.args[0]] = doubles_from_args(instr.args, 1);
        }
        else
            erreur_fatale("Instruction non reconnue.");
    }
}

void vider_dossier_images()
{
    QDir dossier(dossier_travail + "/Images");
    dossier.setNameFilters(QStringList() << "*.*");
    dossier.setFilter(QDir::Files);
    for(QString fichier : dossier.entryList())
    {
        dossier.remove(fichier);
    }
}

SkColor color_from_args(QStringList args)
{
    return SkColorSetARGBInline(
                args[0].toDouble() * 255,
                args[1].toDouble() * 255,
                args[2].toDouble() * 255,
                args[3].toDouble() * 255
                );
}

QVector<double> doubles_from_args(QStringList args)
{
    QVector<double> vec;
    for(QString arg : args)
        vec << arg.toDouble();
    return vec;
}
QVector<double> doubles_from_args(QStringList args, int skip)
{
    QVector<double> vec;
    for(int i = skip; i < args.length(); i++)
        vec << args[i].toDouble();
    return vec;
}

double parse_value(QString chaine)
{
    // Format: $f1$f2$x -> f1(t)+f2(t)+x
    if(chaine[0] == '$')
    {
        double value(0);
        QStringList tokens = chaine.split('$');
        for(int i = 1; i < tokens.length()-1; i++)
            value += funcs[tokens[i]][i_image];
        value += tokens.last().toDouble();
        return value;
    }
    if(chaine[0] == '?')
    {
        double value(1);
        QStringList tokens = chaine.split('?');
        for(int i = 1; i < tokens.length()-1; i++)
            value *= funcs[tokens[i]][i_image];
        value *= tokens.last().toDouble();
        return value;
    }
    else
    {
        return chaine.toDouble();
    }
}

void erreur_fatale(QString message)
{
    std::cout << "Erreur Fatale: " << message.toStdString() << std::endl;
    exit(1);
}
