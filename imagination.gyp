{
 'targets': [
   {
     'configurations': {
       'Debug': { },
       'Release': { }
     },
     'target_name': 'imagination',
     'type': 'executable',
     'dependencies': [
       'third_party/skia/gyp/skia_lib.gyp:skia_lib'
     ],
     'include_dirs': [
       'third_party/skia/include/config',
       'third_party/skia/include/core',
     ],
     'sources': [
       'main.cpp'
     ],
     'ldflags': [
       '-std=c++11' #'-stdlib=libc++', '-lskia', 
     ],
     'cflags': [
       '-Werror', '-W', '-Wall', '-Wextra', '-Wno-unused-parameter', '-g', '-O0'
     ]
   }
 ]
}